<head>
        
 <meta name="page" content="text/html: charset=utf-8">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
  <title> Backend </title>
<style>
body {background: #f69a73;   color: #707981;}
.decor {padding:5px;
  position: relative;
  max-width: 800px;
  background: white;
  border-radius: 30px;
}

.form-left-decoration,
.form-right-decoration {
  content: "";
  position: absolute;
  width: 50px;
  height: 20px;
  background: #f69a73;
  border-radius: 20px;
}
.form-left-decoration {
  bottom: 60px;
  left: -30px;
}
.form-right-decoration {
  top: 60px;
  right: -30px;
}
.form-left-decoration:before,
.form-left-decoration:after,
.form-right-decoration:before,
.form-right-decoration:after {
  content: "";
  position: absolute;
  width: 50px;
  height: 20px;
  border-radius: 30px;
  background: white;
}
.form-left-decoration:before {top: -20px;}
.form-left-decoration:after {
  top: 20px;
  left: 10px;
}
.form-right-decoration:before {
  top: -20px;
  right: 0;
}
.form-right-decoration:after {
  top: 20px;
  right: 10px;
}
.circle {
  position: absolute;
  bottom: 80px;
  left: -55px;
  width: 20px;
  height: 20px;
  border-radius: 50%;
  background: white;
}
.form-inner {padding: 50px;}
.form-inner input,
.form-inner textarea {
  display: block;
  width: 100%;
  padding: 0 20px;
  margin-bottom: 10px;
  background: #E9EFF6;
  line-height: 40px;
  border-width: 0;
  border-radius: 20px;
  font-family: 'Roboto', sans-serif;
}


.form-inner input[type="submit"] {
  margin-top: 30px;
  background: #f69a73;
  border-bottom: 4px solid #d87d56;
  color: white;
  font-size: 14px;
}


.form-inner textarea {resize: none;}
.form-inner h3 {
  margin-top: 0;
  font-family: 'Roboto', sans-serif;
  font-weight: bold;
  font-size: 24px;
  color: #707981;
}

.error {
border: 2px solid red;}

a.butt {
        
        font-size:13px;
        text-decoration: none;
        font-weight: 700;
       padding: 3px 6px;
        background: #eaeef1;
        
        width:60px;
       
        background-image: linear-gradient(rgba(0,0,0,0), rgba(0,0,0,.1));
        border-radius: 3px;
        color: rgba(0,0,0,.6);
        text-shadow: 0 1px 1px rgba(255,255,255,.7);
        box-shadow: 0 0 0 1px rgba(0,0,0,.2), 0 1px 2px rgba(0,0,0,.2), inset 0 1px 2px rgba(255,255,255,.7);
}

a.butt:hover, a.butt.hover {
        background: #fff;
}
a.butt:active, a.butt.active {
        background: #d0d3d6;
        background-image: linear-gradient(rgba(0,0,0,.1), rgba(0,0,0,0));
        box-shadow: inset 0 0 2px rgba(0,0,0,.2), inset 0 2px 5px rgba(0,0,0,.2), 0 1px rgba(255,255,255,.2);
}



</style>
  
    </head>
 <body >   
 <?php
    if (!empty($messages)) {
      print('<div id="messages">');
      // Выводим все сообщения.
      foreach ($messages as $message) {
        print($message);
      }
      print('</div>');
    } ?>
    
 <div class=" container" align="center" style="padding-top:40px; padding-bottom:40px;">
 <p style="color:#fff; font-size:30px;">If you are an ADMIN,  <a href="admin.php" class="butt">click here</a></p>.
 
<form action="" method="POST" class="decor">
  <div class="form-left-decoration"></div>
  <div class="form-right-decoration"></div>
  <div class="circle"></div>
<div class="form-inner">
<h3>WRITE TO US</h3>
<table>
<tr>
<th>
 <div <?php if ($errors['name']) {print 'class="error"';} ?>> <input name="name" value="<?php print $values['name']; ?>" placeholder="FIO"/></div><br/>


<div <?php if ($errors['email']) {print 'class="error"';} ?>> <input name="email" value="<?php print $values['email']; ?>" placeholder="test@mail.ru" /></div>

<br />

<div <?php if ($errors['fieldname']) {print 'class="error"';} ?>> <textarea name="fieldname" placeholder="Tell about yourself" value="<?php print $values['fieldname']; ?>"></textarea></div>

<br />

  Input your year of birth <br/>&emsp;&emsp;&emsp;
  <select name="year" <?php if ($errors['year']) {print 'class="error"';} ?> value="<?php print $values['year']; ?>"> 
  <?php  for ($i=2020;$i>1900;$i--) {?>
  <option value="<?php print($i);?>"><?php print($i);?></option>
  <?php } ?>
  </select>
  <br />
  </th>
        
        <th>&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;</th>
        
        <th>
       
  &emsp;<div  <?php if ($errors['sex']) {print 'class="error"';} ?>>
      <label><input type="radio"  
        name="sex" value="Male" />
        Male</label>&emsp;&emsp;&emsp;
      <label><input type="radio" 
        name="sex" value="Female" />
        Female</label></div><br /><br />
        Select the number of limbs 
       <br />&emsp;
     <div <?php if ($errors['limbs']) {print 'class="error"';} ?>> <label><input type="radio" 
        name="limbs" value="One" />
        1</label>&emsp;&emsp;
      <label><input type="radio" 
        name="limbs" value="Two" />
        2</label>&emsp;&emsp;
        <label><input type="radio" 
        name="limbs" value="Three" />
        3</label>&emsp;&emsp;
        <label><input type="radio" 
        name="limbs" value="Four" />
        4</label></div>
        <br /><br />
        Choose your superpower <br />
   <select name="abilities" multiple <?php if ($errors['abilities']) {print 'class="error"';} ?>> 
  <option value="Immotality">Bessmertie</option>
          <option value="Passage throw walls" >Prohozhdenie skvoz' steni</option>
          <option value="Levitation" >Levitation</option>
          <option value="Mind reading">Chtenie misley</option>
          <option value="Hyperspeed" >Hyperspeed</option>
          <value="<?php print $values['abilities']; ?>">
  </select>

      </th>
      </tr>
      </table>
      <label><input type="checkbox" 
        name="checks" />
      <div <?php if ($errors['checks']) {print 'class="error"';} ?>>  I have read the contract</label></div>
      <br /><br />
  <input  type="submit" value="SEND FORM" />
  </div>
</form>
</div>
</body>
