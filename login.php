<?php

header('Content-Type: text/html; charset=UTF-8');


session_start();

if (!empty($_SESSION['login'])) {
    header('Location: ./');
}


if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    $messages = array();
    $errors = array();
    $errors['login'] = !empty($_COOKIE['login_error']);
    $errors['password'] = !empty($_COOKIE['password_error']);
    
    
    if (!empty($errors['login'])) {
        
        setcookie('login_error', '', 100000);
        
        $messages[] = '<div class="error">Wrong login</div>';
    }
    else if(!empty($errors['password'])){
        
        setcookie('password_error', '', 100000);
        
        $messages[] = '<div class="error">Wrong password </div>';
    }
    ?>
<html lang="ru">
  	<head>
		<meta charset="utf-8">
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">   <meta name="viewport" content="width=device-wedth,initial-scale=1.0">
	
		<title>Login</title>
		
		<style>
		@import url(http://weloveiconfonts.com/api/?family=fontawesome);

@import url(http://meyerweb.com/eric/tools/css/reset/reset.css);
[class*="fontawesome-"]:before {
  font-family: 'FontAwesome', sans-serif;
}

.error{color:#fff;}

body {
	background-color: #2c3338;
	color: #606468;
	font-family: 'Open Sans', Arial, sans-serif;
	font-size: 14px;
	line-height: 1.5em;
}

a {
	color: #eee;
	text-decoration: none;
}

a:hover {
	text-decoration: underline;
}

input {
	border: none;
	font-family: 'Open Sans', Arial, sans-serif;
	font-size: 14px;
	line-height: 1.5em;
	padding: 0;
	-webkit-appearance: none;
}

p {
	line-height: 1.5em;
}

.clearfix { *zoom: 1; } /* For IE 6/7 */
.clearfix:before, .clearfix:after {
	display: table;
	content: "";
}
.clearfix:after { clear: both; }

#login1 {
	margin: 150px auto;
	width: 280px;
}

#login1 form span {
	background-color: #363b41;
	border-radius: 3px 0px 0px 3px;
	-moz-border-radius: 3px 0px 0px 3px;
	-webkit-border-radius: 3px 0px 0px 3px;
	color: #606468;
	display: block;
	float: left;
	height: 50px;
	line-height: 50px;
	text-align: center;
	width: 50px;
}

#login1 form input {
	height: 50px;
	outline:none;
}

#login1 form input[type="text"], input[type="password"] {
	background-color: #3b4148;
	border-radius: 0px 3px 3px 0px;
	-moz-border-radius: 0px 3px 3px 0px;
	-webkit-border-radius: 0px 3px 3px 0px;
	color: #606468;
	margin-bottom: 1em;
	padding: 0 16px;
	width: 198px;
}

#login1 form input:focus {
	color:#fff;
}

#login1 form input[type="submit"] {
	border-radius: 3px;
	-moz-border-radius: 3px;
	-webkit-border-radius: 3px;
	background-color: #ea4c88;
	color: #eee;
	font-weight: bold;
	margin-bottom: 2em;
	text-transform: uppercase;
	cursor:pointer;
	width: 280px;
}

#login1 form input[type="submit"]:hover {
	background-color: #d44179;
}

#login1 > p {
	text-align: center;
}

#login1 > p span {
	padding-left: 5px;
}
		</style>
	</head>
  <?php
    if (!empty($messages)) {
      print('<div id="messages">');

      foreach ($messages as $message) {
        print($message);
      }
      print('</div>');
    }
  ?>
  <div class="container justify-content-center p=0 m=0" id="content">
  <div id="login1">
    <form action="login.php" method="post">
     <fieldset class="clearfix">


     <p><span class="fontawesome-user"></span> <input type="text" name="login" id="login"  value="login" onBlur="if(this.value == '') this.value = 'login'" onFocus="if(this.value == 'login') this.value = ''" required/></p>
      
      <p><span class="fontawesome-lock"></span><input type="password" name="password" id="password" value="password" onBlur="if(this.value == '') this.value = 'password'" onFocus="if(this.value == 'password') this.value = ''" required/></p>
      <p><input type="submit" id="in" value="Enter"/></p>
      
       </fieldset>
    </form>
    </div>
  </div>
</html>
<?php
}

else {
  $errors = FALSE;
    if (empty($_POST['login'])) {

      setcookie('login_error', '1', time() + 24 * 60 * 60);
      $errors = TRUE;
    }
    else {

      setcookie('login_value', $_POST['login'], time() + 30 * 24 * 60 * 60);
    }
    if (empty($_POST['password'])) {
      setcookie('password_error', '1', time() + 24 * 60 * 60);
      $errors = TRUE;
    }
    else{
      setcookie('password_value', $_POST['password'], time() + 30 * 24 * 60 * 60);
    }
    if ($errors) {

      header('Location: login.php');
      exit();
    }
    else{

    setcookie('login_error', '', 100000);
    setcookie('password_error', '', 100000);

    $login = $_POST['login'];
    $password = $_POST['password'];

    $user = 'u20418';
    $pass = '6489094';

    $db = new PDO('mysql:host=localhost;dbname=u20418', $user, $pass);
    extract($_POST);

    try {
      foreach($db->query('SELECT * FROM app12') as $row){
        if($row['login']==$_POST['login']){
            if($row['password']==MD5($_POST['password']))
          // if (password_verify($password, $hash))
            {

            $_SESSION['login'] = $_POST['login'];

            $values['name'] = $row['name'];
            $values['email'] = $row['email'];
            $values['fieldname'] = $row['fieldname'];
            $values['year'] = $row['year'];
            $values['sex'] = $row['sex'];
            $values['limbs'] = $row['limbs'];
            $values['abilities'] = $row['abilities'];
            setcookie('save', '1');

            header('Location: index.php');
          }

          else{
            $errors = TRUE;
            setcookie('password_error', '1s', time() + 24 * 60 * 60);
          }
      }
      }
    }
    catch(PDOException $e){
      print('Error : ' . $e->getMessage());
      exit();
    }
    setcookie('save', '1');

    $errors = TRUE;
    setcookie('login_error', '1', time() + 24 * 60 * 60);

    if ($errors) {
      header('Location: login.php');
      exit();
    }
  }
}
